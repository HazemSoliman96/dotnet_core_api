using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Collections.Generic;
using replication.Models;
using System.Threading.Tasks;
using System.Reflection.Metadata.Ecma335;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace replication.Controllers
{
  [Route("api/[controller]")]
  [ApiController]
    public class StudentController : Controller
    {
        private StudentContext _studentContext;

        public StudentController(StudentContext context) {
          _studentContext = context;
        }

        [HttpGet]
        public ActionResult<IEnumerable<Student>> Get() {
          return _studentContext.Students.ToList();
        }

        [HttpGet]
        [Route("getbyid/{id}")]
        public ActionResult<Student> GetById(int? id) {
          if(id <= 0) {
            return NotFound("Student id must be higher than zero");
          }

          Student student = _studentContext.Students.FirstOrDefault(s => s.StudentId == id);
          if(student == null) {
            return NotFound("Student Not Found");
          }
          return Ok(student);
        }

        [HttpPost]

        public async Task<ActionResult> Post([FromBody]Student student) {
          if(student == null) {
            return NotFound("Student data is not supplied");
          }
          if(!ModelState.IsValid) {
            return BadRequest(ModelState);
          }
          await _studentContext.Students.AddAsync(student);
          await _studentContext.SaveChangesAsync();
          return Ok(student);
        }

        [HttpPut]
        public async Task<ActionResult> Update([FromBody]Student student) {
          if(student == null) {
            return NotFound("Student datais not supplied");
          }
          if(!ModelState.IsValid) {
            return BadRequest(ModelState);
          }
          Student oldStudent = _studentContext.Students.FirstOrDefault(s => s.StudentId == student.StudentId);
          if(oldStudent == null) {
            return NotFound("Student do not exist in the database");
          }
          oldStudent.FirstName = student.FirstName;
          oldStudent.LastName = student.LastName;
          oldStudent.State = student.State;
          oldStudent.City = student.City;
          _studentContext.Attach(oldStudent).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
          await _studentContext.SaveChangesAsync();
          return Ok(oldStudent);
        }


        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int? id) {
          if(id == null) {
            return NotFound("Id is not supplied");
          }

          Student student = _studentContext.Students.FirstOrDefault(s => s.StudentId == id);
          if(student == null) {
            return NotFound("No student found with the particular id supplied");
          }

          _studentContext.Students.Remove(student);
          await _studentContext.SaveChangesAsync();
          return Ok("Student is deleted");
        }
        ~StudentController() {
          _studentContext.Dispose();
        }
    }
}